$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "material_admin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "material_admin"
  s.version     = MaterialAdmin::VERSION
  s.authors     = [""]
  s.email       = ["francis@needham.it"]
  s.homepage    = "http://www.disply.xxx"
  s.summary     = "Summary of MaterialAdmin."
  s.description = "Description of MaterialAdmin."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.5"
  s.add_dependency 'materialize-sass', '~> 0.97'
  s.add_dependency 'will_paginate', '~> 3.1'
  s.add_dependency 'ransack', '~> 1.7'
  s.add_dependency 'simple_form', '~> 3.2'
  s.add_dependency "font-awesome-rails", '~> 4.5'

  s.add_development_dependency "sqlite3"
end