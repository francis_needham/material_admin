// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require materialize-sprockets

var Admin = {

	/**
	 * Init Admin Scripts
	 */
	init: function () {

		$(".button-collapse").sideNav();

		$('select').material_select();

		$('textarea').froalaEditor({
			heightMin: 200
		});
		
		$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 15 // Creates a dropdown of 15 years to control year
		});
		
		$('.sortable').sortable().bind('sortupdate', Admin.updateDragDropImages );
	},


	/**
	 * Ajax call for drag and drop images
	 */
	updateDragDropImages: function () {
		var el_array = []
			
		$('.sortable').children().each(function( index) {
			el_array.push($(this).data('id'))
		})

		$.ajax({
			url: '/admin/ordering',
			method: 'POST',
			data: { ids: el_array },
			success: function(result){
			}
		})
	}
};

window.onload = Admin.init;